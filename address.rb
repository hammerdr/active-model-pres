require 'active_model'

class Address
  include ActiveModel::Validations

  attr_accessor :city, :state, :zip, :address

  validates_presence_of :city, :state, :zip, :address
  validates_acceptance_of :terms_of_service     # only happen on save
  validates_confirmation_of :zip                # only happen on save
  validates_format_of :zip, :with => /[1-9][0-9]{4}/
  validates_inclusion_of :state, :in => ["IL", "CA", "NY"]
  validates_length_of :state, :is => 2
  validates_numericality_of :zip, :greater_than => 11111, :less_than => 89999

  def initialize(args={})
    args.each do |k, v|
      send("#{k}=", v)
    end
  end
end
