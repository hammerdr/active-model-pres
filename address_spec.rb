require './address'

describe Address do
  describe "valid" do
    it "should be valid" do
      address = Address.new(:city => "Chicago", :state => "IL", :zip => 60601, :address => "200 E Randolph")
      address.should be_valid
    end
  end

  describe "invalid" do
    it "should validate presence" do
      address = Address.new
      address.should_not be_valid
    end

    it "should validate length of state" do
      address = Address.new(:city => "Chicago", :state => "Illinois", :zip => 60601, :address => "200 E Randolph")
      address.should_not be_valid
    end

    it "should validate inclusion of state" do
      address = Address.new(:city => "Chicago", :state => "WA", :zip => 60601, :address => "200 E Randolph")
      address.should_not be_valid
    end

    it "should validate numericality of zip" do
      address = Address.new(:city => "Chicago", :state => "IL", :zip => 90000, :address => "200 E Randolph")
      address.should_not be_valid
    end
  end
end
