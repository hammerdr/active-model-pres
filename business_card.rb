require 'active_model'

class BusinessCard
  include ActiveModel::Serialization

  attr_accessor :person
  attr_accessor :address

  def initialize(args={})
    args.each do |k, v|
      send("#{k}=", v)
    end
  end

  def attributes
    { 'person_name' => person.name,
      'city' => address.city,
      'state' => address.state }
  end

  def person_name
    person.name
  end

  def city
    address.city
  end

  def state
    address.state
  end
end
