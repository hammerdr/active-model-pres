require './business_card'
require './person'
require './address'

describe BusinessCard do
  let (:card) {
    BusinessCard.new(:person => Person.new(:name => "Bob Saget"),
                     :address => Address.new(:city => "Portland", :state => "OR"))
  }

  it "should serialize person name" do
    card.serializable_hash["person_name"].should == "Bob Saget"
  end

  it "should serialize city" do
    card.serializable_hash["city"].should == "Portland"
  end

  it "should serialize state" do
    card.serializable_hash["state"].should == "OR"
  end
end
