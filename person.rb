require 'active_model'

class Person
  include ActiveModel::Dirty

  define_attribute_methods [:name]

  attr_reader :name

  alias_method :dirty?, :changed?

  def initialize(args={})
    args.each do |k, v|
      send("#{k}=", v)
    end
  end

  def name=(val)
    name_will_change! unless val == @name
    @name = val
  end

  def save
    @previously_changed = changes
    @changed_attributes.clear
  end
end
