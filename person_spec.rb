require './person'

describe Person do
  it "should not be dirty" do
    person = Person.new(name: "Bill Murray")
    person.save
    person.should_not be_dirty
  end

  it "should not be dirty" do
    person = Person.new
    person.name = "Bob Saget"
    person.should be_dirty
  end
end
