class User
  extend ActiveModel::Callbacks

  define_model_callbacks :sign_in
  # Creates the following callbacks:
  # before_sign_in
  # after_sign_in
  # around_sign_in
  define_model_callbacks :sign_out, :only => :after
  # Creates the following callbacks:
  # after_sign_out

  before_sign_in :sign_out
  after_sign_out :party

  def sign_in
    # execute callbacks
    run_callbacks :sign_in do
      Warden.create_session_for(self)
    end
  end

  def sign_out
    # execute callbacks
    run_callbacks :sign_out do
      Warden.destroy_session_for(self)
    end
  end

  def party; end
end
