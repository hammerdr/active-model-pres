class UserObserver < ActiveModel::Observer
  def after_sign_in(user)
    user.party
  end
end
